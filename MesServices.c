/* rajout de services personnalisés */

#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>

#include <zephyr/sys/printk.h>
#include <zephyr/sys/byteorder.h>
#include <zephyr/kernel.h>
#include <zephyr/init.h>
#include <zephyr/sys/__assert.h>
#include <zephyr/types.h>
#include <zephyr/sys/printk.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/services/bas.h>
#include <zephyr/bluetooth/services/hrs.h>

#include <zephyr/logging/log.h>

#include "MesServices.h"

static uint8_t led_state = 1U;
static uint8_t btn_state = 0U;
static char version [] = "EMP.1.0";

/*
 * Fonction read pour la led
 */

static ssize_t read_led(struct bt_conn *conn,
			       const struct bt_gatt_attr *attr, void *buf,
			       uint16_t len, uint16_t offset)
{

	ssize_t ret = bt_gatt_attr_read(conn, attr, buf, len, offset, &led_state,
				 sizeof(led_state));
	printk("led : 0%02x\n", led_state);
	return ret;
}

/*
 * Fonction write pour la led
 */

static ssize_t write_led(struct bt_conn *conn,
			       const struct bt_gatt_attr *attr, void *buf,
			       uint16_t len, uint16_t offset)
{
	memcpy(&led_state, buf, sizeof(btn_state));
	printk("led write : 0%02x\n", led_state);
	return 0;
}

/*
 * Fonction read pour le bouton
 */

static ssize_t read_btn(struct bt_conn *conn,
			       const struct bt_gatt_attr *attr, void *buf,
			       uint16_t len, uint16_t offset)
{
	ssize_t ret = bt_gatt_attr_read(conn, attr, buf, len, offset, &btn_state,
				 sizeof(btn_state));
	printk("btn read : 0%02x\n", btn_state);
	return ret;
}

/*
 * Fonction write pour le bouton
 */

static ssize_t write_btn(struct bt_conn *conn,
			       const struct bt_gatt_attr *attr, void *buf,
			       uint16_t len, uint16_t offset)
{
	memcpy(&btn_state, buf, sizeof(btn_state));
	printk("btn write : 0%02x\n", btn_state);
	return 0;
}

/*
 * Fonction read pour la version
 */

static ssize_t read_vers(struct bt_conn *conn,
			       const struct bt_gatt_attr *attr, void *buf,
			       uint16_t len, uint16_t offset)
{

	ssize_t ret = bt_gatt_attr_read(conn, attr, buf, len, offset, version,
				 sizeof(version));
	printk("version : %s\n", version);
	return ret;
}

uint8_t get_led_state()
{
	return led_state;
}

void set_btn_state(uint8_t value)
{
	btn_state = value;
}

//Service d'interface

BT_GATT_SERVICE_DEFINE(inter,
	BT_GATT_PRIMARY_SERVICE(BT_UUID_INTERFACE),
	BT_GATT_CHARACTERISTIC(BT_UUID_LED,
			       BT_GATT_CHRC_READ | 
				   BT_GATT_CHRC_WRITE ,
			       BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, read_led, write_led,
			       &led_state),

	BT_GATT_CHARACTERISTIC(BT_UUID_BTN,
			       BT_GATT_CHRC_READ, 
				   //BT_GATT_CHRC_WRITE| BT_GATT_CHRC_NOTIFY,
			       BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, read_btn, write_btn,
			       &btn_state),

);

//Service System

BT_GATT_SERVICE_DEFINE(syst,
	BT_GATT_PRIMARY_SERVICE(BT_UUID_SYSTEM),
	BT_GATT_CHARACTERISTIC(BT_UUID_VERSION,
			       BT_GATT_CHRC_READ,
			       BT_GATT_PERM_READ | BT_GATT_PERM_WRITE, read_vers, NULL,
			       version),
);

static int init(void)
{
	return 0;
}


SYS_INIT(init, APPLICATION, CONFIG_APPLICATION_INIT_PRIORITY);
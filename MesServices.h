#ifndef MES_SERVICES_H_
#define MES_SERVICES_H_

/**********************************************/
/*                     LED                    */
/**********************************************/

/**
 *  @brief Service INTERFACE
 */
#define BT_UUID_INTERFACE BT_UUID_DECLARE_16(0x1500)

/**
 *  @brief LED 
 */
#define BT_UUID_LED BT_UUID_DECLARE_16(0x1501)

/**
 *  @brief BTN
 */
#define BT_UUID_BTN BT_UUID_DECLARE_16(0x1502)

/**
 *  @brief Service SYSTEM
 */
#define BT_UUID_SYSTEM BT_UUID_DECLARE_16(0x1600)

/**
 *  @brief VERSION 
 */
#define BT_UUID_VERSION BT_UUID_DECLARE_16(0x1601)


uint8_t get_led_state(void);

void set_btn_state(uint8_t value);


#endif /* MES_SERVICES_H_ */
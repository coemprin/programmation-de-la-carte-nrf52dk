# Programmation de la carte nRF52DK

Ce projet vise à implémenter une interface BLE sur la carte nRF52DK :
Une fois le code compilé et flashé sur la carte, deux nouveaux services Bluetooth seront ajoutés. (0x1600 et 0x1500)

# Fonctionnalités :

    -La LED1 clignote
    -La LED2 (0x1501) peut être allumée et éteinte depuis l'application nRFConnect (envoyer 0x00 pour l'éteindre et 0x01 j'usqu'à 0xFF pour l'allumer)
    -Le BUTTON1 (0x1502) modifie sa valeur dans l'application nRFConnect lorsqu'il est enfoncé (0x00 si non pressé et 0x01 si pressé)
    -La valeur 0x1601 est lisible, il s'agit de la version. Je n'ai pas réussi à faire apparaitre un String sur l'application mais il s'agit du code ASCII pour "EMP.1.0" .


# Etapes

    -Installez nRF Connect sur votre ordinateur puis ouvrez-le.
    -Installez les applications Bluetooth Low Energy et ToolChain Manager puis ouvrez ce dernier.
    -Installez nRF Connect SDK v2.5.0 puis ouvrez le avec VScode (installer les autres prérequis demandés).
    -Une fois dans VsCode installez les Extensions de nRF Connect.
    -Creez une nouvelle application : copiez l'extension peripheral_hr, Mettez cette copie à la racine de votre ordinateur.
    -Copiez les fichiers .c .h fournis dans ce gitlab dans le dossier src (remplacez le fichier main.c déjà présent) et remplacez le fichier prj.conf par celui présent dans ce Gitlab.
    -Creez un build configuration avec la nordic board nrf52dk_nrf52832 et une configuration prj.conf.
    -Brancez votre carte à votre ordinateur, Build puis Flash.
    -Ouvrez nRF Connect sur votre smartphone, puis connectez vous en Bluetooth au device nommé "CORENTIN EMPRIN" et récupérez ou écrivez des données.


(Je n'ai pas réussi à creer un gitlab sur ma zone de travail puisque cela créait un nouveau dossier et que, depuis ce dossier la compilation ne fonctionnait pas: erreur sur le Cmake)

# Annexes

Tableau des interfaces BLE:

![texte](tableau_interface_BLE.png)

Je n'ai pas implémenté les autres caractéristiques mais cela ressemblerait beaucoup à ce qui a été déjà fait sur la led ou le bouton.

# Remarque

Le code est repris et modifié depuis le fichier bas.c (que l'on trouve dans le sample peripheral_hr) qui gère l'affichage de la batterie. Les fonctions read et write, les déclarations de services sont tirés de ce fichier.

Les fonctions du sample button (exemple d'utilisation du bouton) sont aussi utilisés pour gérer les leds et le bouton.

Plusieurs warnings se déclenchent lors de la compilation. (Il peut s'agir de fonction non utilisées).

# Conclusion

Je n'ai pas implémenté toutes les caractéristiques, mais je pense avoir réaliser une (petite) base du keyfinder:
- Une action sur l'application change une donnée sur l'objet embarqué.
- Une action sur l'objet embarqué change une donnée sur l'application.

